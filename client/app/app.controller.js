/**
 * Client side code.
 */

(function() {
    
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http"];

    function RegCtrl($http) {
        var regCtrlself = this; 
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.onlyFemale = onlyFemale;
        regCtrlself.initForm = initForm;

        regCtrlself.user = {

        };

        regCtrlself.empty = {
            
        };

        regCtrlself.nationalities = [
            { name: "Please select", value: 0},
            { name: "Singaporean", value: 1},
            { name: "Indonesian", value: 2},
            { name: "Thai", value: 3},
            { name: "Malaysian", value: 4},
            { name: "Vietnamese", value: 5}
        ];


        function onSubmit() {
            console.log(regCtrlself.user.selectedNationality);
            //angular.copy(regCtrlself.user, regCtrlself.entry);
            console.log(regCtrlself.user);
            //console.log(regCtrlself.entry);

            $http.post("/submit", regCtrlself.user)
            .then(function (result) {
                console.log("Submission is succesful");
            }).catch(function(e){
                console.log("Submission is unsuccesful");
            });

            regCtrlself.showResult = true; 
        };

        function initForm() {
            regCtrlself.user.selectedNationality = "0";
            //regCtrlself.user.gender = "F";
        };

        //custom validation
        function onlyFemale() {
            console.log("Only female");
            return regCtrlself.user.gender == "F";
        };

        regCtrlself.initForm();

        regCtrlself.reset = function() {
            regCtrlself.user = angular.copy(regCtrlself.empty);
          };
       
    }

    

})();






/**
 * Starter code 

(function () {
    
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http"];

    function RegCtrl($http) {

    }

})();

 */